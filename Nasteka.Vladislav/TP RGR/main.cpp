#include "Header.h"

int main() {
    setlocale(LC_ALL, "Russian");

    Dictionary dictionary;

    dictionary.addTranslation("hello", "������");
    dictionary.addTranslation("apple", "������");
    dictionary.addTranslation("moon", "����");
    dictionary.addTranslation("stars", "�����");
    dictionary.addTranslation("shine", "������");
    dictionary.addTranslation("pomegranate", "������");

    std::cout << "������� 'moon': " << dictionary.getTranslation("moon") << std::endl;
    std::cout << "������� 'hello': " << dictionary.getTranslation("hello") << std::endl;
    std::cout << "������� 'apple': " << dictionary.getTranslation("apple") << std::endl;
    
    std::cout << "������� ���� �������: " << std::endl;
    dictionary.printAllTranslations();

    std::cout << "������ ��������� ��������� �� �������" << std::endl << "�������� �������� 'apple': ";
    dictionary.removeTranslation("apple");
    std::cout << "�������� �������� 'stars': ";
    dictionary.removeTranslation("stars");
    std::cout << "�������� �������� 'orange': ";
    dictionary.removeTranslation("orange");

    std::cout << "����� ������� ���� �������: " << std::endl;
    dictionary.printAllTranslations();
    std::cout << "������� ����� � ������� �� ���������" << std::endl;
    dictionary.addWordsFromFile("dictionary.txt");

    std::cout << "������� ���� ������� ��� �������� ���������� ���� �� ���������:" << std::endl;
    dictionary.printAllTranslations();

    return 0;
}
