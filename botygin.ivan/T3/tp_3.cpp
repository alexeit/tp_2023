﻿#include <iostream>

#include <sstream>
#include <algorithm>
#include <fstream>
#include <iterator>

#include <unordered_map>
#include <string>
#include <set>
#include <map>
#include <vector>

typedef std::unordered_map<std::string, std::set<int>> HashTable;

struct Command
{
    std::string arg1;
    std::string arg2;
};

std::istream& operator>>(std::istream& in, Command& dest)
{
    if (!std::istream::sentry(in)) return in;

    in >> dest.arg1;
    std::getline(in, dest.arg2, '\n');
    if (dest.arg2.size() > 0)
        dest.arg2 = dest.arg2.substr(1, dest.arg2.size());
    else
        dest.arg2 = "";
    return in;
}

void runCommands(HashTable& hashTable, const std::vector<Command> commands, std::ostream& out)
{
    for (std::size_t i = 0; i < commands.size(); i++)
    {
        Command command = commands.at(i);
        out << command.arg1 << ' ';
        out << command.arg2 << ": ";
        if (command.arg1 == "READ")
        {
            std::ifstream file(command.arg2);

            if (!file.is_open())
            {
                out << "Не удалось открыть файл." << '\n';
                continue;
            }
            out << '\n';

            std::string line;
            int lineNumber = 1;

            while (std::getline(file, line))
            {
                out  << lineNumber << ' ' << line << '\n';
                std::string word;
                std::istringstream iss(line);

                while (iss >> word)
                {
                    word.erase(std::remove_if(word.begin(), word.end(), [](unsigned char c) { return !std::isalnum(c); }), word.end());
                    std::transform(word.begin(), word.end(), word.begin(), [](unsigned char c) { return std::tolower(c); });

                    if (!word.empty()) hashTable[word].insert(lineNumber);
                }

                lineNumber++;
            }

            file.close();
            out << '\n';
        }
        else if (command.arg1 == "PRINT")
        {
            out << '\n';
            std::map<std::string, std::set<int>> table;

            for (const auto& pair : hashTable)
                table.insert(pair);

            for (const auto& pair : table)
            {
                out << pair.first << ": ";
                for (const auto& lineNumber : pair.second)
                    out << lineNumber << " ";

                out << '\n';
            }
            out << '\n';
        }
        else if (command.arg1 == "FIND")
        {
            auto iterator = hashTable.find(command.arg2);
            if (iterator != hashTable.end())
            {
                std::set<int> lines = iterator->second;
                for (const auto& line : lines)
                    out << line << " ";
                out << '\n';
            }
            else
                out << "Не удалось найти слово.\n";
        }
        else if (command.arg1 == "DELETE")
        {
            auto iterator = hashTable.find(command.arg2);
            if (iterator != hashTable.end())
            {
                hashTable.erase(iterator);
                out << "Слово успешно удалено.\n";
            }
            else
                out << "Не удалось найти слово.\n";

        }
        else if (command.arg1 == "INSERT")
        {
            std::istringstream in(command.arg2);
            std::string word;
            int line;
            if (in >> word >> line)
            {
                hashTable[word].insert(line);
                out << "Слово успешно добавлено.\n";
            }
            else
                out << "Неверные аргументы.\n";
        }
        else if (command.arg1 == "CLEAR")
        {
            hashTable.clear();
            out << "Таблица успешно очищена.\n";
        }
        else
            out << "Команда не найдена";
    }
}

int main() {
    setlocale(LC_ALL, "RU");

    std::istringstream inputCommands(
        "READ Text1.txt\n"
        "PRINT\n"
        "FIND at\n"
        "FIND the\n"
        "FIND 123\n"
        "DELETE the\n"
        "DELETE the\n"
        "INSERT 123 15\n"
        "INSERT 123\n"
        "INSERT 123 w\n"
        "FIND 123\n"
        "PRINT\n"
        "CLEAR\n"
        "PRINT\n"
        "READ Text2\n"
        "READ Text2.txt\n"
        "PRINT\n"
    );

    std::vector<Command> commands;
    while (!inputCommands.eof()) {
        std::copy(
            std::istream_iterator< Command >(inputCommands),
            std::istream_iterator< Command >(),
            std::back_inserter(commands)
        );
        bool eofFlag = inputCommands.eof();
        inputCommands.clear();
        if (eofFlag) {
            inputCommands.setstate(std::ios::eofbit);
        }
        inputCommands.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }


    std::unordered_map<std::string, std::set<int>> HashTable;
    runCommands(HashTable, commands, std::cout);
    return 0;
}