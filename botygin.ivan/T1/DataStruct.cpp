#include "DataStruct.h"

std::istream& operator>>(std::istream& in, DelimiterIO&& dest)
{
    // ��� ���������� ���������� �����/������ ������ ���������� � �������� ���������� ������ sentry
    if (!std::istream::sentry(in)) return in;
    char c = '0';
    in >> c;
    if (in && (c != dest.exp))
    {
        in.setstate(std::ios::failbit);
    }
    return in;
}

std::istream& operator>>(std::istream& in, KeyIO&& dest)
{
    if (!std::istream::sentry(in)) return in;

    if (!std::getline(in >> DelimiterIO{ ':' }, dest.name, ' '))
    {
        in.setstate(std::ios::failbit);
    }
    return in;
}

std::istream& operator>>(std::istream& in, DoubleIO&& dest)
{
    if (!std::istream::sentry(in)) return in;
    return in >> std::scientific >> dest.ref;
}

std::istream& operator>>(std::istream& in, StringIO&& dest)
{
    if (!std::istream::sentry(in)) return in;
    return std::getline(in >> DelimiterIO{ '"' }, dest.ref, '"');
}

std::istream& operator>>(std::istream& in, HexIO&& dest)
{
    if (!std::istream::sentry(in)) return in;
    return in >> DelimiterIO{ '0' } >> DelimiterIO{ 'X' } >> std::hex >> dest.ref;
}

std::istream& operator>>(std::istream& in, Data& dest)
{
    if (!std::istream::sentry(in)) return in;
    Data data;
    in >> DelimiterIO{ '{' };
    for (int i = 0; i < 3; i++)
    {
        std::string key;
        in >> KeyIO{ key };

        if (key == "key1")
        {
            if (!(in >> DoubleIO{ data.key1 }))
                return in;
        }
        else if (key == "key2")
        {
            if (!(in >> HexIO{ data.key2 }))
                return in;
        }
        else if (key == "key3")
        {
            if (!(in >> StringIO{ data.key3 }))
                return in;
        }
        else
        {
            in.setstate(std::ios::failbit);
            return in;
        }
    }
    in >> DelimiterIO{ ':' } >> DelimiterIO{ '}' };

    if (in) dest = data;
    return in;
}

std::ostream& operator<<(std::ostream& out, const Data& src)
{
    if (!std::ostream::sentry(out)) return out;
    out << "{";
    out << ":key1 " << std::setprecision(2) << std::scientific << src.key1;
    out << ":key2: 0x" << std::hex << std::uppercase << src.key2 << std::nouppercase;
    out << ":key3 \"" << src.key3;
    out << "\":}";
    return out;
}

bool operator<(const Data& data1, const Data& data2)
{
    if (data1.key1 != data2.key1)
        return data1.key1 < data2.key1;

    if (data1.key2 != data2.key2)
        return (data1.key2 < data2.key2);

    return data1.key3.length() < data2.key3.length();
}