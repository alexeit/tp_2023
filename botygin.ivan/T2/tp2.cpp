﻿#include <iostream>
#include <climits>
#include <sstream>
#include <string>
#include <cassert>
#include <iterator>
#include <vector>
#include <iomanip>
#include <algorithm>
#include <array>
#include <set>
#include <functional>
#include <numeric>
#include <cmath>

struct Point
{
    int x, y;
};

struct Polygon
{
    std::vector<Point> points;
};

struct Command
{
    std::string arg1;
    std::string arg2;
};

struct DelimiterIO
{
    char exp;
};

struct PointIO
{
    Point& point;
};

std::istream& operator>>(std::istream& in, DelimiterIO&& dest)
{
    if (!std::istream::sentry(in)) return in;
    char c = '0';
    in >> c;
    if (in && (c != dest.exp))
    {
        in.setstate(std::ios::failbit);
    }
    return in;
}

std::istream& operator>>(std::istream& in, PointIO&& dest)
{
    if (!std::istream::sentry(in)) return in;
    std::string x, y;
    if (!std::getline(in >> DelimiterIO{ '(' }, x, ';'))
    {
        in.setstate(std::ios::failbit);
        return in;
    }

    if (!std::getline(in, y, ')'))
    {
        in.setstate(std::ios::failbit);
        return in;
    }
    
    dest.point.x = std::stoi(x);
    dest.point.y = std::stoi(y);

    return in;
}

std::istream& operator>>(std::istream& in, Polygon& dest)
{
    if (!std::istream::sentry(in)) return in;
    
    int points;
    Polygon poly;

    try
    {
        in >> points;
    }
    catch (const std::exception&)
    {
        in.setstate(std::ios::failbit);
        return in;
    }

    for (int i = 0; i < points && in; i++)
    {
        Point point;
        in >> PointIO{ point };
        poly.points.push_back(point);
    }

    if (in && in.peek() == '\n')
    {
        dest = poly;
    }
    else
    {
        in.setstate(std::ios::failbit);
    }
    return in;
}

std::istream& operator>>(std::istream& in, Command& dest)
{
    if (!std::istream::sentry(in)) return in;

    in >> dest.arg1;
    std::getline(in, dest.arg2, '\n');
    dest.arg2 = dest.arg2.substr(1, dest.arg2.size());
    return in;
}

std::ostream& operator<<(std::ostream& out, const Polygon& src)
{
    if (!std::ostream::sentry(out)) return out;
    out << src.points.size();

    for (std::size_t i = 0; i < src.points.size(); i++)
    {
        out << ' ' << '(' << src.points[i].x << ';' << src.points[i].y << ')';
    }
    return out;
}

bool isEqual(const Point& p1, const Point& p2)
{
    return p1.x == p2.x && p1.y == p2.y;
}

bool operator==(const Polygon& p1, const Polygon& p2)
{
    if (p1.points.size() == p2.points.size())
    {
        auto iterator = std::mismatch(p1.points.begin(), p1.points.end(),
            p2.points.begin(), p2.points.end(), isEqual);
        return (iterator.first == p1.points.end());
    }
    else
    {
        return false;
    }
}

double getArea(const Polygon& polygon)
{
    if (polygon.points.size() < 3) return 0.0;
    std::vector<Point>::const_iterator prev = polygon.points.end() - 1;
    std::vector<double> points(polygon.points.size());
    std::transform(polygon.points.begin(), polygon.points.end(), points.begin(),
        std::bind([](const Point& v1, const Point& v2)
            { return v1.x * v2.y - v2.x * v1.y; }, std::placeholders::_1, *prev));
    double result = std::accumulate(points.begin(), points.end(), 0.0);
    return std::abs(result) / 2.0;
}

void runCommands(const std::vector<Polygon> polygons, const std::vector<Command> commands, std::ostream& out)
{
    for (std::size_t i = 0; i < commands.size(); i++)
    {
        if (commands.at(i).arg1 == "AREA")
        {
            out << "AREA";
            if (commands.at(i).arg2 == "ODD")
            {
                std::vector<Polygon> figures;
                std::copy_if(polygons.begin(), polygons.end(), std::back_inserter(figures), [](const Polygon& polygon)
                    { return polygon.points.size() % 2 == 0; });

                std::vector<double> areas(polygons.size());
                std::transform(figures.begin(), figures.end(), areas.begin(), std::bind(getArea, std::placeholders::_1));
                out << " ODD: " << std::accumulate(areas.begin(), areas.end(), 0.0) << '\n';
            }
            else if (commands.at(i).arg2 == "EVEN")
            {
                std::vector<Polygon> figures;
                std::copy_if(polygons.begin(), polygons.end(), std::back_inserter(figures), [](const Polygon& polygon)
                    { return polygon.points.size() % 2 == 1; });

                std::vector<double> areas(polygons.size());
                std::transform(figures.begin(), figures.end(), areas.begin(), std::bind(getArea, std::placeholders::_1));
                out << " EVEN: " << std::accumulate(areas.begin(), areas.end(), 0.0) << '\n';
            }
            else if (commands.at(i).arg2 == "MEAN")
            {
                std::vector<double> areas(polygons.size());
                std::transform(polygons.begin(), polygons.end(), areas.begin(), std::bind(getArea, std::placeholders::_1));
                out << " MEAN: " << std::accumulate(areas.begin(), areas.end(), 0.0) / areas.size() << '\n';
            }
            else
            {
                try
                {

                    std::size_t points = stoi(commands.at(i).arg2);

                    std::vector<Polygon> figures;
                    std::copy_if(polygons.begin(), polygons.end(), std::back_inserter(figures), [points](const Polygon& polygon)
                        { return polygon.points.size() == points; });

                    std::vector<double> areas(polygons.size());
                    std::transform(figures.begin(), figures.end(), areas.begin(), std::bind(getArea, std::placeholders::_1));
                    std::cout << ' ' << points << " points: " << std::accumulate(areas.begin(), areas.end(), 0.0) << '\n';
                }
                catch (const std::exception&)
                {
                    out << ": Wrong argument (" << commands.at(i).arg2 << ")\n";
                }
            }
        }
        else if (commands.at(i).arg1 == "MAX")
        {
            out << "MAX";
            if (commands.at(i).arg2 == "AREA")
            {
                std::vector<double> areas(polygons.size());
                std::transform(polygons.begin(), polygons.end(), areas.begin(), std::bind(getArea, std::placeholders::_1));
                out << " AREA: " << *std::max_element(areas.begin(), areas.end()) << '\n';
            }
            else if (commands.at(i).arg2 == "VERTEXES")
            {
                std::vector<int> vertexes(polygons.size());
                std::transform(polygons.begin(), polygons.end(), vertexes.begin(), [](const Polygon& polygon)
                    { return polygon.points.size();  });
                out << " VERTEXES: " << *std::max_element(vertexes.begin(), vertexes.end()) << '\n';
            }
            else
            {
                out << ": Wrong argument (" << commands.at(i).arg2 << ")\n";
            }
        }
        else if (commands.at(i).arg1 == "MIN")
        {
            out << "MIN";
            if (commands.at(i).arg2 == "AREA")
            {
                std::vector<double> areas(polygons.size());
                std::transform(polygons.begin(), polygons.end(), areas.begin(), std::bind(getArea, std::placeholders::_1));

                out << " AREA: " << *std::min_element(areas.begin(), areas.end()) << '\n';
            }
            else if (commands.at(i).arg2 == "VERTEXES")
            {
                std::vector<int> vertexes(polygons.size());
                std::transform(polygons.begin(), polygons.end(), vertexes.begin(), [](const Polygon& polygon)
                    { return polygon.points.size();  });
                out << " VERTEXES: " << *std::min_element(vertexes.begin(), vertexes.end()) << '\n';

            }
            else
            {
                out << ": Wrong argument (" << commands.at(i).arg2 << ")\n";
            }
        }
        else if (commands.at(i).arg1 == "COUNT")
        {
        out << "COUNT";
            if (commands.at(i).arg2 == "ODD")
            {
                std::vector<Polygon> figures;
                std::copy_if(polygons.begin(), polygons.end(), std::back_inserter(figures), [](const Polygon& polygon)
                    { return polygon.points.size() % 2 == 0; });

                std::cout << " ODD: " << figures.size() << '\n';
            }
            else if (commands.at(i).arg2 == "EVEN")
            {
                std::vector<Polygon> figures;
                std::copy_if(polygons.begin(), polygons.end(), std::back_inserter(figures), [](const Polygon& polygon)
                    { return polygon.points.size() % 2 == 1; });

                std::cout << " EVEN: " << figures.size() << '\n';

            }
            else
            {
                try
                {

                    std::size_t points = stoi(commands.at(i).arg2);

                    std::vector<Polygon> figures;
                    std::copy_if(polygons.begin(), polygons.end(), std::back_inserter(figures), [points](const Polygon& polygon)
                        { return polygon.points.size() == points; });

                    std::cout << ' ' << points << " points: " << figures.size() << '\n';
                }
                catch (const std::exception&)
                {
                    out << ": Wrong argument (" << commands.at(i).arg2 << ")\n";
                }
            }
        }
        else if (commands.at(i).arg1 == "MAXSEQ")
        {
            try
            {
                std::istringstream iss(commands.at(i).arg2 + '\n');
                Polygon polygon;
                iss >> polygon;

                if (polygon.points.size() == 0)
                {
                    out << "MAXSEQ: Wrong argument (" << commands.at(i).arg2 << ")\n";
                    continue;
                }

                std::vector<size_t> figures(polygons.size());
                std::transform(figures.begin(), figures.end(), figures.begin(),
                    [](size_t& figure)
                    {
                        figure = 0;
                        return figure;
                    });
                std::transform(polygons.begin(), polygons.end(), figures.begin(),
                    std::bind([polygon](const Polygon& f1, size_t& f2)
                        { return (f1 == polygon) ? ++f2 : 0; }, std::placeholders::_1, * (figures.end() - 1)));

                std::cout << "MAXSEQ " << polygon << ": " << *std::max_element(figures.begin(), figures.end()) << '\n';
            }
            catch (const std::exception&)
            {
                out << ": Wrong argument (" << commands.at(i).arg2 << ")\n";
            }
        }
        else if (commands.at(i).arg1 == "LESSAREA")
        {
            try
            {
                std::istringstream iss(commands.at(i).arg2 + '\n');
                Polygon polygon;
                iss >> polygon;

                if (polygon.points.size() == 0)
                {
                    out << "LESSAREA: Wrong argument (" << commands.at(i).arg2 << ")\n";
                    continue;
                }
                
                double area = getArea(polygon);
                std::vector<Polygon> figures;
                std::copy_if(polygons.begin(), polygons.end(), std::back_inserter(figures), [area](const Polygon& polygon)
                    { return getArea(polygon) < area; });

                std::vector<double> areas(polygons.size());
                std::transform(figures.begin(), figures.end(), areas.begin(), std::bind(getArea, std::placeholders::_1));

                std::cout << "LESSAREA " << polygon << ": " << figures.size() << '\n';
            }
            catch (const std::exception&)
            {
                out << ": Wrong argument (" << commands.at(i).arg2 << ")\n";
            }
        }
        else
        {
            out << "Wrong command (" << commands.at(i).arg1 << ' ' << commands.at(i).arg2 << ")\n";
        }
    }
}

int main()
{
    std::istringstream inputPolygons(
        "3 (3;3) (1;1) (1;3)\n"
        "4 (3;3) (1;1) (1;3) (4;3)\n"
        "2 (3;3) (1;1) (1;3)\n"
        "2 (3;3) (1;1) (1;3)\n"
        "2 (3;3) (1;1) (1;3)\n"
        "2 (3;3) (1;1) (1;3)\n"
        "3 (1;3) (1;1) (1;3)\n"
        "3 (1;3) (1;1) (1;3)\n"
        "3 (1;3) (1;1) (1;3)\n"
        "3 (1;3) (1;1) (1;3)\n"
        "3 (1;3) (1;1) (1;3)\n"
        "3 (1;3) (1;1) (1;3)\n"
        "4 (3;3) (12;1) (7;3) (2;3)\n"
        "6 (3;3) (12;1) (7;3) (2;3) (5;6) (1;5)\n"
    );

    std::istringstream inputCommands(
        "AREA ODD\n"                    //чётные
        "AREA EVEN\n"                   //нечётные
        "AREA MEAN\n"                   //средняя
        "AREA 3\n"                      //3 вершины
        "AREA asdsfasf\n"               //неверная команда
        "MAX AREA\n"                    //максимальная площадь
        "MAX VERTEXES\n"                //максимальное кол-во вершин
        "MAX asdsfasf\n"                //неверная команда
        "MIN AREA\n"                    //минимальная площадь
        "MIN VERTEXES\n"                //минимальное кол-во вершин
        "MIN asdsfasf\n"                //неверная команда
        "COUNT ODD\n"                   //чётные
        "COUNT EVEN\n"                  //нечётные
        "COUNT 3\n"                     //конкретное
        "COUNT awdwadwa\n"              //неверная команда
        "MAXSEQ 3 (3;3) (1;1) (1;3)\n"  //максимальное кол-во идущих подряд фигур
        "MAXSEQ Wrong Args\n"           //неверная команда
        "Wrong command\n"               //неверная команда
        "LESSAREA 3 (3;3) (1;1) (1;3)\n"//фигура с площадью меньше указанной
        "LESSAREA 3 (3;3) (1;1)\n"//неверная команда
    );

    std::vector<Polygon> polygons;
    while (!inputPolygons.eof()) {
        std::copy(
            std::istream_iterator< Polygon >(inputPolygons),
            std::istream_iterator< Polygon >(),
            std::back_inserter(polygons)
        );
        bool eofFlag = inputPolygons.eof();
        inputPolygons.clear();
        if (eofFlag) {
            inputPolygons.setstate(std::ios::eofbit);
        }
        inputPolygons.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }

    std::vector<Command> commands;
    while (!inputCommands.eof()) {
        std::copy(
            std::istream_iterator< Command >(inputCommands),
            std::istream_iterator< Command >(),
            std::back_inserter(commands)
        );
        bool eofFlag = inputCommands.eof();
        inputCommands.clear();
        if (eofFlag) {
            inputCommands.setstate(std::ios::eofbit);
        }
        inputCommands.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }

    std::copy(
        std::begin(polygons),
        std::end(polygons),
        std::ostream_iterator< Polygon >(std::cout, "\n")
    );

    runCommands(polygons, commands, std::cout);
    return 0;
}