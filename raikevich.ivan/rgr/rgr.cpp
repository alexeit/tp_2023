﻿#include <iostream>
#include <fstream>
#include "Hash.hpp"

int main() {
    FrequencyDictionary dictionary;
    std::string text;

    std::ifstream inputFile("input.txt");
    if (!inputFile) {
        std::cout << "Failed to open the input file." << std::endl;
        return 1;
    }

    std::string word;
    while (inputFile >> word) {
        dictionary.insert(word);
    }
    inputFile.close();

    std::cout << "Three most frequent words:\n";
    std::cout << dictionary.getThreeMostFrequentWords();
    std::cout << "____________________________________________" << std::endl;
    std::cout << "search data" << std::endl;
    std::cout << dictionary.search("data") << std::endl;
    std::cout << "____________________________________________" << std::endl;
    std::cout << "remove data" << std::endl;
    dictionary.remove("data");
    std::cout << "____________________________________________" << std::endl;
    std::cout << dictionary.getThreeMostFrequentWords();
    std::cout << "____________________________________________" << std::endl;
    std::cout << "search data" << std::endl;
    std::cout << dictionary.search("data") << std::endl;
    std::cout << "____________________________________________" << std::endl;
    return 0;
}

