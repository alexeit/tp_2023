#include "DataStruct.h"
int main()
{
    using nspace::DataStruct;

    std::vector< DataStruct > data;
    std::istringstream iss("(:key2 0365:key1 10.6E+13:key3 \"first\":)\n(:key3 5.01e-3:key2 0363:key1 \"second\":)\n(:key1 5.01E-6:key2 0361:key3 \"third\":)\n(:key1 5.01e-6:key2 0391:key3 \"forth\":)\n(:key3 \"fifth\":key1 5.01e+6:key2 0351:)\n(:key1 5.01e-6:key2 0361:key3 \"sixth:)\n(:key1 5.01e-6:key2 0361:key3 \"sixth\":)\n(:key1 6.02e-6:key2 03322:key3 \"seventh\":)\n( :key1 5.01e-6 :key2 0361 :key3 \"eighth\":)\n(:key1 4.01e3:key2 0311:key3 \"nineth\":)\n(:key2 5.01e-6:key2 0361:key3 \"tenth\":)\n(:key1 5.023e-6:key2 0461:key3 \"eleventh\":)\n(:key1 3.01e-6:key2 0361:key4 \"tvelfth\":)\n(:key1 4.11e-6:key2 0331:key3 \"thirteenth\":)\n(:key1 5:key2 0361:key3 \"forteenth\":)\n(:key1 4.11e-6:key2 0331:key3 \"fifteenth\":)\n(:key1 4.11e-6:key2 0b001:key3 \"sixteenth\":)\n(:key1 4.11e-6:key2 0301:key3 \"seventeenth\":)\n(:key1 4.11e-6:key2 0331:key3 \"ei\"gh\"teenth\":)\n(:key1 5.11e-6:key2 0331:key3 \"nineteenth\":)\n//(:key1 4.11e-6:key2 0331:key3 \"twentieth\":)\n(:key1 5.11e-6:key2 331:key3 \"twenty first\":)\n(:\n");
    do
    {
        if (!iss)
        {
            iss.clear();
            iss.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        std::copy(
            std::istream_iterator< DataStruct >(iss),
            std::istream_iterator< DataStruct >(),
            std::back_inserter(data)
        );
    } while (!iss.eof());

    std::sort
    (
        data.begin(), 
        data.end(), 
        [](DataStruct a, DataStruct b)
        { 
            return sort(a, b); 
        }
    );

    std::cout << "Data:\n";
    std::copy(
        std::begin(data),
        std::end(data),
        std::ostream_iterator< DataStruct >(std::cout, "\n")
    );

    return 0;
}