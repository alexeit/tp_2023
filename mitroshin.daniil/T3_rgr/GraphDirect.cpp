#include "GraphDirect.h"

GraphDirect::GraphDirect(const int& vertices) : numberOfVertices(vertices) {
    if (vertices < 0) {
        throw std::length_error("Invalid number of vertices for graph\n");
        return;
    }
    matrix.resize(numberOfVertices, std::vector<bool>(numberOfVertices, false));
}
GraphDirect::GraphDirect() : numberOfVertices(0) {
    matrix.resize(numberOfVertices, std::vector<bool>(numberOfVertices, false));
}
int GraphDirect::getVertices() {
    return this->numberOfVertices;
}

GraphDirect::GraphDirect(const GraphDirect& other) {
    numberOfVertices = other.numberOfVertices;
    matrix = other.matrix;
}

bool GraphDirect::addArc(const int& source, const int& destination) {
    if ((source < 0 || source > this->numberOfVertices) || (destination < 0 || source > this->numberOfVertices) || (source == destination)) {
        throw std::invalid_argument("Invalid vertex\n");
    }
    if (matrix[source][destination]) return false;
    matrix[source][destination] = true;
    return true;
}

bool GraphDirect::isArcInTheGraph(const int& from, const int& to) {
    if ((from < 0 && from >= numberOfVertices) || (to < 0 && to > numberOfVertices)) {
        throw std::length_error("Vertex is not in range\n");
    }
    return (matrix[from][to]);
}

bool GraphDirect::removeArc(const int& source, const int& destination) {
    if ((source < 0 || source > this->numberOfVertices) || (destination < 0 || source > this->numberOfVertices)) {
        throw std::invalid_argument("Invalid vertex\n");
    }
    if (!matrix[source][destination]) return false;
    matrix[source][destination] = false;
    return true;
}

void GraphDirect::addVertex() {
    int newSize = numberOfVertices + 1;
    std::vector<std::vector<bool>> newMatrix(newSize, std::vector<bool>(newSize, false));

    for (int i = 0; i < numberOfVertices; i++) {
        for (int j = 0; j < numberOfVertices; j++) {
            newMatrix[i][j] = matrix[i][j];
        }
    }

    matrix = newMatrix;

    numberOfVertices++;
}
bool GraphDirect::isVertexInTheGraph(const int& N) {
    return (N < numberOfVertices&& N > -1);
}


bool GraphDirect::removeVertex(const int& vertex) {
    if (numberOfVertices == 0) {
        throw std::length_error("Graph is empty\n");
    }
    else if (vertex < 0) {
        throw std::invalid_argument("Invalid vertex\n");
    }
    if (vertex > this->numberOfVertices) return false;
    matrix.erase(matrix.begin() + vertex);
    for (auto& row : matrix) {
        row.erase(row.begin() + vertex);
    }
    numberOfVertices--;
    return true;
}

bool GraphDirect::loopFinder(const int& node, std::vector<bool>& visited, std::vector<bool>& stack) {
    visited[node] = true;
    stack[node] = true;

    for (int neighbor = 0; neighbor < int(matrix[node].size()); neighbor++) {
        if (matrix[node][neighbor]) {
            if (!visited[neighbor]) {
                if (loopFinder(neighbor, visited, stack)) {
                    return true;
                }
            }
            else if (stack[neighbor]) {
                return true;
            }
        }
    }

    stack[node] = false;
    return false;
}

bool GraphDirect::hasCycle() {
    size_t n = matrix.size();
    std::vector<bool> visited(n, false);
    std::vector<bool> stack(n, false);

    for (int node = 0; node < int(n); ++node) {
        if (!visited[node]) {
            if (loopFinder(node, visited, stack)) {
                return true;
            }
        }
    }

    return false;
}

void GraphDirect::topologicalSortUtil(const int& vertex, std::vector<bool>& isVisited, std::stack<int>& result) {
    isVisited[vertex] = true;

    for (int i = 0; i < int(matrix[vertex].size()); i++) {
        if (matrix[vertex][i] && !isVisited[i]) {
            topologicalSortUtil(i, isVisited, result);
        }
    }

    result.push(vertex);
}



bool GraphDirect::topologicalSort() {
    if (numberOfVertices == 0) {
        throw std::length_error("Sort is inappropriate for empty graph\n");
    }
    if (this->hasCycle()) {
        return false;
    }
    std::vector<bool> visited(numberOfVertices, false);


    std::stack<int> result;
    for (int i = 0; i < numberOfVertices; i++) {
        if (!visited[i]) {
            topologicalSortUtil(i, visited, result);
        }
    }

    std::cout << "Topological Sort: ";
    while (!result.empty()) {
        std::cout << result.top() << " ";
        result.pop();

    }

    std::cout << std::endl;
    return true;
}

void GraphDirect::DFSUtil(const int& vertex, std::vector<bool>& visited) {
    visited[vertex] = true;
    std::cout << vertex << " ";

    for (int i = 0; i < int(matrix[vertex].size()); i++) {
        if (matrix[vertex][i] && !visited[i])
            DFSUtil(i, visited);
    }
}

void GraphDirect::DFS(const int& startVertex) {
    if (startVertex < 0 || startVertex > this->numberOfVertices) {
        throw std::invalid_argument("Invalid vertex\n");
    }
    std::vector<bool> isVisited(numberOfVertices, false);

    std::cout << "DFS traversal starting from vertex " << startVertex << ": ";
    DFSUtil(startVertex, isVisited);
    std::cout << std::endl;
}

void GraphDirect::printGraph() {
    std::cout << "   ";
    for (int i = 0; i < numberOfVertices; i++) {
        std::cout << i << "  ";
    }
    std::cout << std::endl;

    for (int i = 0; i < numberOfVertices; i++) {
        std::cout << i << " ";

        for (int j = 0; j < numberOfVertices; j++) {
            if (j >= 10) {
                if (i < 10) {
                    std::cout << std::setw(2) << " ";
                    std::cout << matrix[i][j] << " ";
                }
                else {
                    std::cout << std::setw(0) << ' ';
                    std::cout << matrix[i][j] << "  ";
                }
            }
            if (j < 10) {
                if (i < 10) {
                    std::cout << " ";
                    std::cout << matrix[i][j] << " ";
                }
                else {
                    std::cout << matrix[i][j] << "  ";
                }
            }



        }

        std::cout << std::endl;
    }
}

bool GraphDirect::isEmpty() {
    return (numberOfVertices == 0);
}

GraphDirect& GraphDirect::operator=(GraphDirect&& other) noexcept {
    if (this != &other) {
        numberOfVertices = other.numberOfVertices;
        matrix = std::move(other.matrix);

    }
    return *this;
}