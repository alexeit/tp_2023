﻿// TP2_Skripkar.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//
#include <numeric>
#include <algorithm>
#include <vector>
#include <map>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cctype>
#include <iterator>
#include<math.h>
#include<cmath>

struct Point
{
    int x, y;
};

struct Polygon
{
    std::vector< Point > points;
};

class ErrorData : public std::exception
{
private:
    const char* reason_;
public:
    ErrorData(const char* info) : reason_(info) {}
    const char* what() const noexcept { return reason_; }
};

std::map<std::string, size_t> optionsList = { {"EVEN", 1}, {"ODD", 2}, {"MEAN", 3}, {"AREA", 4}, {"VERTEXES", 5} };

void readCommand(std::istringstream& input, std::vector<Polygon>& figures);

void runCommandArea(std::string& opt, std::vector<Polygon>& figures);
void runCommandMin(std::string& opt, std::vector<Polygon>& figures);
void runCommandMax(std::string& opt, std::vector<Polygon>& figures);
void runCommandCount(std::string& opt, std::vector<Polygon>& figures);
void runCommandRightshapes(/*[[maybe_unused]]*/ /*std::string& opt,*/ std::vector<Polygon>& polygons, std::istringstream& command);
void runCommandInframe(std::string& opt, std::vector<Polygon>& figures, std::istringstream& input);

double getArea(const Polygon& polygon);

bool searchRightshapes(const Polygon& poly);
bool compareByX(const Point& p1, const Point& p2);
bool compareByY(const Point& p1, const Point& p2);

std::istream& operator>>(std::istream& in, Polygon& dest);
bool operator==(const Point& p1, const Point& p2);
bool operator==(const Polygon& poly1, const Polygon& poly2);


int main()
{
    std::vector <Polygon> figures;
    std::ifstream fileData;
    try
    {
        std::string errorString;

      

        fileData.open("Test_shapes.txt");
        if (!fileData)
        {
            throw ErrorData("THE SPECIFIED FILE WAS NOT FOUND");
        }
        std::cout << "DATA IS BEING ENTERED...\n";

        auto isFilled = [&](const Polygon& poly) {return poly.points.size() != 0; };
        std::copy_if(
            std::istream_iterator< Polygon >(fileData),
            std::istream_iterator< Polygon >(),
            std::back_inserter(figures),
            isFilled
        );

        fileData.close();

        if (figures.empty())
        {
            throw ErrorData("THE DESCRIPTION OF THE FIGURES WAS NOT FOUND.\n");
        }
    }

    catch (std::exception& e)
    {
        std::cerr << "ERROR! " << e.what() << "\n";
        std::cerr << "FORCED TERMINATION OF THE PROGRAM.\n";
        exit(-1);
    }

    auto print = [](const Polygon& n)
    {
        auto printPoint = [](const Point& p)
        {
            std::cout << "(" << p.x << ";" << p.y << ")";
        };

        std::for_each(n.points.begin(), n.points.end(), printPoint);
        std::cout << "\n";
    };

    std::cout << "#-------------------------------#\n";
    std::cout << "THE RESULTING SHAPES:\n";
    std::for_each(figures.cbegin(), figures.cend(), print);
    std::cout << "#-------------------------------#\n\n";
    std::cout << "PUT THE COMMAND BELOW:\n";

    std::string commandLine;

    std::cout << "$";
    std::ifstream test("Test.txt");
    while (std::getline(test, commandLine))
    {
        std::istringstream input(commandLine);
        readCommand(input, figures);
    }

    return 0;
}

void readCommand(std::istringstream& input, std::vector<Polygon>& figures)
{
    std::map<std::string, size_t> commandsList = { {"AREA", 1}, {"MAX", 2}, {"MIN",3}, {"COUNT", 4}, {"RIGHTSHAPES", 5}, {"INFRAME", 6} };

    std::string command;
    std::string opt;
    input >> command;

    if (commandsList[command] < 5)
    {
        input >> opt;
    }

    switch (commandsList[command])
    {
    case 1:
    {
        runCommandArea(opt, figures);
        break;
    }
    case 2:
    {
        runCommandMin(opt, figures);
        break;
    }
    case 3:
    {
        runCommandMax(opt, figures);
        break;
    }
    case 4:
    {
        runCommandCount(opt, figures);
        break;
    }
    case 5:
    {
        runCommandRightshapes(figures, input);
        break;
    }
    case 6:
    {
        runCommandInframe(opt, figures, input);
        break;
    }
    default:
    {
        std::cout << "<INVALID COMMAND>\n";
        break;
    }

    }

    std::cout << "$";
}


void runCommandArea(std::string& opt, std::vector<Polygon>& figures)
{
  
    auto area = [&figures](auto condition)
    {
        return std::accumulate(figures.begin(), figures.end(), 0.0,
           [&](double sum, const Polygon& p)
            {
                return sum + (condition(p) ? getArea(p) : 0.0);
            });
    };

    switch (optionsList[opt])
    {
    case 1:
    case 2:
    {
        bool parity = (opt == "ODD") ? 1 : 0;
        auto sumArea = area([&](const Polygon& poly) {return poly.points.size() % 2 == parity; });
        std::cout << "Total " << opt << " area: " << sumArea << "\n";
        break;
    }

    case 3:
    {
        std::cout << "Average area of figures: " << (area([&](const Polygon& poly) {return poly == poly; }) / figures.size()) << "\n";
        break;
    }

    default:
    {
        size_t isNumber = std::count_if(opt.begin(), opt.end(), isdigit);
        if (isNumber == opt.size())
        {
            int numberOfVertexes = std::stoi(opt);
            auto sumArea = area([&](const Polygon& poly) { return poly.points.size() == static_cast<std::vector<Point>::size_type>(numberOfVertexes); });

            /*auto sumArea = area([&] {return poly.points.size() == static_cast<std::vector<Point>::size_type>(numberOfVertexes); });*/
            /*auto sumArea = area([&] {return poly.points.size() == static_cast<int>(numberOfVertexes); });*/
            /*auto sumArea = area([&](const Polygon& poly) {return poly.points.size() == numberOfVertexes; });*/
            std::cout << "Area of polygons with " << numberOfVertexes << " vertexes: " << sumArea << "\n";
        }
        else
        {
            std::cout << "No such option was found. \nAREA options are\n";
            std::cout << "\t\tODD \n\t\tEVEN \n\t\tMEAN \n\t\t<num-of-vertexes> \n";
        }
        break;
    }
    }
}

void runCommandMin(std::string& opt, std::vector<Polygon>& figures)
{
    switch (optionsList[opt])
    {
    case 4:
    {
        auto maxArea = std::max_element(figures.begin(),
            figures.end(),
            [](const Polygon& poly1, const Polygon& poly2) {return getArea(poly1) < getArea(poly2); });
        std::cout << "Max area: " << getArea(*maxArea) << "\n";
        break;
    }

    case 5:
    {
        auto maxVertexes = std::max_element(figures.begin(),
            figures.end(),
            [](const Polygon& poly1, const Polygon& poly2) {return poly1.points.size() < poly2.points.size(); });
        std::cout << "Max vertexes number: " << maxVertexes->points.size() << "\n";
        break;
    }
    default:
    {
        std::cout << "No such option was found. \nMAX options are\n";
        std::cout << "\t\tAREA \n\t\tVERTEXES\n";
        break;
    }

    }
}

void runCommandMax(std::string& opt, std::vector<Polygon>& figures)
{
    switch (optionsList[opt])
    {
    case 4:
    {
        auto maxArea = std::min_element(figures.begin(),
            figures.end(),
            [](const Polygon& poly1, const Polygon& poly2) {return getArea(poly1) < getArea(poly2); });
        std::cout << "Min area: " << getArea(*maxArea) << "\n";
        break;
    }

    case 5:
    {
        auto maxVertexes = std::min_element(figures.begin(),
            figures.end(),
            [](const Polygon& poly1, const Polygon& poly2) {return poly1.points.size() < poly2.points.size(); });
        std::cout << "Min vertexes number: " << maxVertexes->points.size() << "\n";
        break;
    }
    default:
    {
        std::cout << "No such option was found. \nMIN options are\n";
        std::cout << "\t\tAREA \n\t\tVERTEXES\n";
        break;
    }

    }
}

void runCommandCount(std::string& opt, std::vector<Polygon>& figures)
{
    switch (optionsList[opt])
    {
    case 1:
    case 2:
    {
        bool parity = (opt == "ODD") ? 1 : 0;
        std::cout << "Figures with " << opt << " number of vertexes: " << std::count_if(figures.begin(), figures.end(), [parity](const Polygon& poly) {return poly.points.size() % 2 == parity; }) << "\n";
        break;
    }
    default:
    {
        size_t isNumber = std::count_if(opt.begin(), opt.end(), isdigit);
        if (isNumber == opt.size())
        {
            int numberOfVertexes = std::stoi(opt);
            auto countPolygons = count_if(figures.begin(),
                figures.end(),
                ([&](const Polygon& poly) {return poly.points.size() == static_cast<std::vector<Point>::size_type>(numberOfVertexes); }));
                /*([&](const Polygon& poly) {return poly.points.size() == numberOfVertexes; }));*/
            std::cout << "Number of polygons with " << numberOfVertexes << " vertexes: " << countPolygons << "\n";
        }
        else
        {
            std::cout << "No such option was found. \nCOUNT options are\n";
            std::cout << "\t\tODD \n\t\tEVEN \n\t\t<num-of-vertexes> \n";
        }
        break;
    }

    }
}

bool searchRightshapes(const Polygon& poly)
{
    std::vector<Point> temp(poly.points.size() + 2);
    temp[0] = poly.points[poly.points.size() - 1];
    temp[temp.size() - 1] = poly.points[0];

    std::copy(poly.points.begin(), poly.points.end(), temp.begin() + 1);
    size_t cnt = 0, size = temp.size();
    return std::any_of(temp.begin(), temp.end(), [&](auto& point)
//    return std::any_of(temp.begin(), temp.end(), [&]()
        {
            if (cnt == 0 || cnt == size - 1)
            {
                cnt++;
                return false;
            }
            Point A = temp[cnt - 1];
            Point B = temp[cnt];
            Point C = temp[cnt + 1];
            double distAB = sqrt(pow(B.x - A.x, 2) + pow(B.y - A.y, 2));
            double distAC = sqrt(pow(C.x - A.x, 2) + pow(C.y - A.y, 2));
            double distBC = sqrt(pow(C.x - B.x, 2) + pow(C.y - B.y, 2));
            double epsilon = 0.00001;
            cnt++;

            return std::abs(distAB * distAB + distBC * distBC - distAC * distAC) < epsilon && point == point;
        });
}

void runCommandRightshapes(/*[[maybe_unused]]*/ /*std::string& opt,*/ std::vector<Polygon>& polygons, std::istringstream& command)
//void runCommandRightshapes(std::string& opt, std::vector<Polygon>& polygons, std::istringstream& command)
{
    std::string ending;
    command >> ending;
    if (!ending.empty())
    {
        std::cerr << "Invalid command\\n" << "\n";
        return;
    }
    int count = std::count_if(polygons.begin(), polygons.end(), searchRightshapes);
    std::cout << count << "\n";
}



void runCommandInframe(std::string& opt, std::vector<Polygon>& figures, std::istringstream& input)
{
    input.ignore();
    opt = input.peek();
    auto isNumber = std::count_if(opt.begin(), opt.end(), isdigit);

    if (isNumber)
    {
        Polygon inframePolygon;
        input >> inframePolygon;

        auto framePoints = std::minmax_element(inframePolygon.points.begin(), inframePolygon.points.end(), [](const Point& p1, const Point& p2) {return ((p1.x - p2.x) + (p1.y - p2.y)) < 0; });
        auto frameLeft = *framePoints.first;
        auto frameRight = *framePoints.second;

        auto minmaxX = std::minmax_element(figures.begin(), figures.end(),
            [](const Polygon& a, const Polygon& b)
            {
                return std::min_element(a.points.begin(), a.points.end(), compareByX)->x
                    < std::min_element(b.points.begin(), b.points.end(), compareByX)->x;
            });

        auto minmaxY = std::minmax_element(figures.begin(), figures.end(),
            [](const Polygon& a, const Polygon& b)
            {
                return std::min_element(a.points.begin(), a.points.end(), compareByY)->y
                    < std::min_element(b.points.begin(), b.points.end(), compareByY)->y;
            });

        Point minPointX = *std::min_element(minmaxX.first->points.begin(), minmaxX.first->points.end(), compareByX);
        Point maxPointX = *std::max_element(minmaxX.second->points.begin(), minmaxX.second->points.end(), compareByX);

        Point minPointY = *std::min_element(minmaxY.first->points.begin(), minmaxY.first->points.end(), compareByY);
        Point maxPointY = *std::max_element(minmaxY.second->points.begin(), minmaxY.second->points.end(), compareByY);

        if (minPointX.x < frameLeft.x && minPointY.y < frameLeft.y && frameRight.x < maxPointX.x && frameRight.y < maxPointY.y)
        {
            std::cout << "<TRUE>\n";
        }
        else
        {
            std::cout << "<FALSE>\n";
        }

    }
    else
    {
        std::cout << "Wrong polygon input. \nECHO has the following structure:\n";
        std::cout << "\t\t'INFRAME <POLYGON>'\n";
        std::cout << "\t\t<POLYGON> has structure 'n (x1;y1) (x2;y2) ...(xn;yn)' where 'n' is number of points.\n";
    }
}

double getArea(const Polygon& polygon)
{
    double area = 0.0;
    size_t j = polygon.points.size() - 1;

    for (std::vector<Point>::size_type i = 0; i < polygon.points.size(); i++/*int i = 0; i < polygon.points.size(); i++*/) {
        area += (polygon.points[j].x + polygon.points[i].x) * (polygon.points[j].y - polygon.points[i].y);
        j = i;
    }

    return std::abs(area / 2.0);
}

bool compareByX(const Point& p1, const Point& p2)
{
    return p1.x < p2.x;
}
bool compareByY(const Point& p1, const Point& p2)
{
    return p1.y < p2.y;
}

std::istream& operator>>(std::istream& in, Polygon& dest)
{
    std::istream::sentry sentry(in);
    if (!sentry)
    {
        return in;
    }

    int numberOfPoints = 0;
    in >> numberOfPoints;

    Polygon input;
    try
    {
        if (numberOfPoints == 0)
        {
            throw ErrorData("Wrong polygon size information.");
        }

        input.points.resize(numberOfPoints);

        for (auto& i : input.points)
        {
            in.ignore();
            if (in.get() != '(')
                throw ErrorData("Wrong polygon point information.");
            in >> i.x;
            if (in.get() != ';')
                throw ErrorData("Wrong polygon point information.");
            in >> i.y;
            if (in.get() != ')')
                throw ErrorData("Wrong polygon point information.");
        }

        dest = input;
    }
    catch (std::exception& e)
    {
        in.clear();
        std::string errorString;
        std::getline(in, errorString, '\n');
        std::cerr << "Error! -> " << errorString << "  <-  " << e.what() << "\n";
        dest = input;
        return in;
    }
    return in;
}

bool operator==(const Polygon& poly1, const Polygon& poly2)
{
    return std::equal(poly1.points.begin(), poly1.points.end(), poly2.points.begin(), poly2.points.end());
}

bool operator==(const Point& p1, const Point& p2)
{
    return (p1.x == p2.x) && (p1.y == p2.y);
}