#ifndef DICTIONARY_H
#define DICTIONARY_H

#include <string>
#include <vector>
#include <algorithm>
#include <map>

class FrequencyDictionary
{
private:
	std::map<std::string, size_t> dictionary;
public:
	FrequencyDictionary();
	~FrequencyDictionary();
	bool searchWord(std::string& key);
	bool insertWord(std::string& key);
	bool removeWord(std::string& key);
	std::string toLower(const std::string& str);
	void threeMostFrequentWords(std::pair<std::string, size_t>* arr);
	void printDictionary();
	void printMostFrequentWords();
 };

FrequencyDictionary::FrequencyDictionary() : dictionary() {}

FrequencyDictionary::~FrequencyDictionary() {}

std::string FrequencyDictionary::toLower(const std::string& str) {
	std::string result = str;
	for (char& c : result) {
		c = std::tolower(static_cast<unsigned char>(c));
	}
	return result;
}

bool FrequencyDictionary::searchWord(std::string& key) {

	key = toLower(key);
	return dictionary.find(key) != dictionary.end();

}
bool FrequencyDictionary::insertWord(std::string& key) {

	key = toLower(key);
	if ((dictionary.find(key)) != dictionary.end())
	{
		dictionary.find(key)->second++;
	}
	else
	{
		dictionary.insert({ key, 1 });
	}
	return true;
}
bool FrequencyDictionary::removeWord (std::string& key)
{
	key = toLower(key);
	if (dictionary.erase(key))
	{
		return true;
	}
	return false;
}
void FrequencyDictionary::threeMostFrequentWords(std::pair<std::string, size_t>* arr)
{
	auto it = dictionary.begin();
	auto endIt = dictionary.end();
	size_t minMaxThree = std::min({ arr[0].second, arr[1].second, arr[2].second });

	while (it != endIt)
	{
		if (it->second >= minMaxThree)
		{
			size_t minIndex = 0;
			size_t i = 0;
			while (true)
			{
				if (arr[i].second == minMaxThree)
				{
					minIndex = i;
					break;
				}
				i++;
			}

			arr[minIndex] = *it;
			minMaxThree = std::min({ arr[0].second, arr[1].second, arr[2].second });
		}

		++it;
	}
}
void FrequencyDictionary::printMostFrequentWords()
{
	std::pair<std::string, size_t> threeWords[3];
	threeMostFrequentWords(threeWords);
	if (threeWords[0].second == 0 || threeWords[1].second == 0 || threeWords[2].second == 0)
	{
		std::cout << "The dictionary lacks sufficient words to form a list of the three most frequently occurring words.\n";
	}
	else
	{
		std::cout << "Three most frequently occurring words:\n";
		std::cout << threeWords[0].first << " - " << threeWords[0].second << "\n";
		std::cout << threeWords[1].first << " - " << threeWords[1].second << "\n";
		std::cout << threeWords[2].first << " - " << threeWords[2].second << "\n";
	}
}
void FrequencyDictionary::printDictionary()
{
	if (dictionary.empty()) {
		throw std::runtime_error("Dictionary is empty.");
	}

	for (const auto& entry : dictionary)
	{
		std::cout << entry.first << " - " << entry.second << "\n";
	}
}
#endif