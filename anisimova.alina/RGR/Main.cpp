#include <iostream>
#include <fstream>
#include <sstream>
#include "Dictionary.h"


bool isFilename(std::string& word)
{
    if (word.size() < 4)
    {
        return false;
    }
    return word.substr(word.size() - 4) == ".txt";
}

int main() {
   FrequencyDictionary dictionary;
    std::string command;
    std::string word;
    std::string text;
    std::string filename;
    std::istringstream iss1 ("INSERTFROMFILE text.txt\n\
        PRINT\n\
        SEARCH apple\n\
        INSERT apple\n\
        SEARCH apple\n\
        REMOVE apple\n\
        SEARCH apple\n\
        TOP3\n\
        INSERT on on on on on on on on on on on on on on on on on on\n\
        TOP3");
    while (!iss1.eof())
    {
        std::getline(iss1, text);
        std::istringstream iss(text);
        iss >> command;
        if (command == "SEARCH" || command == "INSERT" || command == "REMOVE" || command == "TOP3" || command == "INSERTFROMFILE" || command == "PRINT")
        {
            if (command == "INSERT")
            {
                size_t count = 0;
                while (!iss.eof())
                {
                    iss >> word;
                    if (dictionary.insertWord(word))
                    {
                        count++;
                    }
                    else
                    {
                        continue; 
                    }
                }
                std::cout << "The number of words successfully read from your text is " << count << '\n';
            }
            else if (command == "SEARCH")
            {
                iss >> word;
                    if (dictionary.searchWord(word) == 0)
                    {
                        std::cout << "<NO SUCH WORD IN DICTIONARY>\n";
                    }
                    else
                    {
                        std::cout << "The word \"" << word << "\" occured " << dictionary.searchWord(word) << " times\n";
                    }
                
            }
            else if (command == "REMOVE")
            {
                iss >> word;

                    if (dictionary.removeWord(word))
                    {
                        std::cout << "The word \"" << word << "\" was deleted from the dictionary\n";
                    }
                    else
                    {
                        std::cout << "<NO SUCH WORD IN DICTIONARY>\n";
                    }
            }
            else if (command == "TOP3")
            {
                dictionary.printMostFrequentWords();
            }
            else if (command == "INSERTFROMFILE")
            {
                iss >> filename;
                std::ifstream inputFile(filename);
                if (!inputFile.is_open())
                {
                    std::cerr << "<FILE OPENING ERROR>\n";
                    iss.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                    continue;
                }
                else
                {
                    size_t count = 0;
                    while (!inputFile.eof())
                    {
                        while (inputFile >> word) {
                            word.erase(std::remove_if(word.begin(), word.end(), [](char c) { return !std::isalpha(c); }), word.end());

                            if (!word.empty()) {
                                if (dictionary.insertWord(word)) {
                                    count++;
                                }
                            }
                        }
                    }
                    inputFile.close();
                    std::cout << "The number of words successfully read from your text is " << count << '\n';
                }
            }
            else if (command == "PRINT")
            {
                std::cout << "==================================DICTIONARY==================================\n"; 
                dictionary.printDictionary();
            }
        }
        else
        {
            std::cout << "<INVALID COMMAND>\n";
            iss.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
    }
    return 0;
}