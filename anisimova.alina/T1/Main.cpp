#include "TP1.h"
int main()
{
    using nspace::Data;
    using nspace::CompareDataStruct;

    std::vector< Data > data;
    std::istringstream iss("(:key1 3.9d:key2 `c`:key3 \"Right\":)\n\
                            (:key1 3.9:key2 `v`:key3 \"No\":)\n\
                            (:key2 `13:key1 3.9dD:key3 \"NO\":) \n\
                            (:key1 4.8d:key2 `M`:key3 \"Correct\":)\n\
                            (:key1 3.9d:key2 89LL:key3 \"Wrong:)\n\
                            (:key1 3.9d:key3 \"no\":)\n\
                            (:key2 `F`:key3 \"Yeah\":key1 0.1D:)\n\
                        ");

    while (!iss.eof())
    {
        if (!iss)
        {
            iss.clear();
        }
        std::copy(
            std::istream_iterator< Data >(iss),
            std::istream_iterator< Data >(),
            std::back_inserter(data)
        );
    }

    std::sort(
        std::begin(data),
        std::end(data),
        CompareDataStruct()
    );

    std::cout << "Data:\n";
    std::copy(
        std::begin(data),
        std::end(data),
        std::ostream_iterator< Data >(std::cout, "\n")
    );

    return 0;
}